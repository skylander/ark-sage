/**
 * Mathematical classes and implementations required for L1 optimization.
 * 
 * @author Yanchuan Sim
 * @version 0.1
 */
package edu.cmu.cs.ark.yc.utils;

