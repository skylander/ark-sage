/**
 * A collection of easy to use tools for common uses of SAGE.
 * <p>
 * Todo in future releases
 * <ul>
 * <li>LDA replacement</li>
 * <li>SAGE with topic models</li>
 * <li>Variational approximation implementation for SAGE</li>
 * </ul>
 * 
 * @author Yanchuan Sim
 * @version 0.1
 */
package edu.cmu.cs.ark.sage.apps;

